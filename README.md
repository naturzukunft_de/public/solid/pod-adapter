# Feature branches
**Start a feature**  
`mvn gitflow:feature-start -DpushRemote=true`  

**Finish a feature**  
`mvn gitflow:feature-finish -DfeatureSquash=true`  
