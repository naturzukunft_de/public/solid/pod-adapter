package de.naturzukunft.rdf.solid.pod.adapter;

import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.client.WebClient;

class BasicContainerBuilder extends DefaultResourceBuilder {

	public BasicContainerBuilder(DPopProvider dPopProvider, WebClient webClient) {
		super(dPopProvider, webClient);
		this.withLink("<http://www.w3.org/ns/ldp#BasicContainer>; rel=\"type\"");
	}

	public BasicContainerBuilder(DPopProvider dPopProvider, WebClient webClient, String clientId, String accessToken) {
		super(dPopProvider, webClient, clientId, accessToken);
		this.withLink("<http://www.w3.org/ns/ldp#BasicContainer>; rel=\"type\"");
	}

	@Override
	protected HttpMethod getHttpMethod() {
		return HttpMethod.POST;
	}

	@Override
	public PodResponse build() {
		boolean hasContent = hasContent(); 
		if(!hasContent) {
			setContent(String.format("<> <http://purl.org/dc/terms/title> \"%s\" .", getSlug()));
		}
		PodResponse podResponse = super.build();
		if(!hasContent) {
			setContent(null);
		}
		return podResponse;
	}
}
