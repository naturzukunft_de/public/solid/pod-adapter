package de.naturzukunft.rdf.solid.pod.adapter;

import java.net.URI;

import org.springframework.http.HttpMethod;

public interface DPopProvider {
	String getDPopToken(String userName, HttpMethod httpMethod, URI uri);
}