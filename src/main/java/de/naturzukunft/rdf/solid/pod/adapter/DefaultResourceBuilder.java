package de.naturzukunft.rdf.solid.pod.adapter;

import java.net.URI;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

class DefaultResourceBuilder implements ResourceBuilder {

	private DPopProvider dPopProvider;
	private String content;
	private String link;
	private String slug;
	private String url;
	private String token;
	private String clientId;
	private WebClient webClient;
	
	public DefaultResourceBuilder(DPopProvider dPopProvider, WebClient webClient) {
		this.dPopProvider = dPopProvider;
		this.webClient = webClient;
	}

	public DefaultResourceBuilder(DPopProvider dPopProvider, WebClient webClient, String clientId, String accessToken) {
		this(dPopProvider, webClient);
		this.clientId = clientId;
		this.token = accessToken;
	}
	
	@Override
	public ResourceBuilder withContent(String content) {
		this.content = content;
		return this;
	}

	@Override
	public ResourceBuilder withClientId(String clientId) {
		this.clientId = clientId;
		return this;
	}

	@Override
	public ResourceBuilder withToken(String token) {
		this.token = token;
		return this;
	}
	
	@Override
	public ResourceBuilder withUrl(String url) {
		this.url = url;
		return this;
	}

	@Override
	public ResourceBuilder withLink(String link) {
		this.link = link;
		return this;
	}

	@Override
	public ResourceBuilder withSlug(String slug) {
		this.slug = slug;
		return this;
	}

	@Override
	public PodResponse build() {
		
		Mono<ResponseEntity<String>> podMono = webClient
				.method(getHttpMethod()).uri(this.url.toString())
				.header("Content-Type", "text/turtle")
				.header("Authorization", "DPoP " + token)
				.header("Link", link)
				.header("Slug", slug)
				.header("DPoP", dPopProvider.getDPopToken(clientId, getHttpMethod(), URI.create(url)))
				.bodyValue(content)
				.retrieve()
				.toEntity(String.class);
		ResponseEntity<String> responseEntity = podMono.block();
		
		return new PodResponse(responseEntity.getStatusCode(), responseEntity.getHeaders(), responseEntity.getBody());
	}
	
	protected HttpMethod getHttpMethod() {
		return HttpMethod.PUT;
	}
//	private ExchangeFilterFunction logRequest() {
//	    return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
//	        if (log.isDebugEnabled()) {
//	            StringBuilder sb = new StringBuilder("Request: \n");
//	            sb.append(clientRequest.method()).append("\n");
//	            sb.append(clientRequest.url()).append("\n");
//	            clientRequest
//	              .headers()
//	              .forEach((name, values) -> {
//	            	  sb.append("Header: " + name + " - " + values).append("\n");
//	              }
//	    		);
//	            log.debug(sb.toString());
//	        }
//	        return Mono.just(clientRequest);
//	    });
//	}
//	
//	private ExchangeFilterFunction logResponse() {
//	    return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
//	        if (log.isDebugEnabled()) {
//	            StringBuilder sb = new StringBuilder("Response: \n");
//	            //append clientRequest method and url
//	            Headers headers = clientResponse.headers();
//	            headers.asHttpHeaders().forEach((name, values) -> values.forEach(value -> log.debug( "-"+ value)));
//	            log.debug(sb.toString());
//	        }
//	        return Mono.just(clientResponse);
//	    });
//	}

	protected String getContent() {
		return content;
	}

	protected void setContent(String content) {
		this.content = content;
	}

	protected String getLink() {
		return link;
	}

	protected void setLink(String link) {
		this.link = link;
	}

	protected String getSlug() {
		return slug;
	}

	protected void setSlug(String slug) {
		this.slug = slug;
	}

	protected String getUrl() {
		return url;
	}

	protected void setUrl(String url) {
		this.url = url;
	}

	protected String getToken() {
		return token;
	}

	protected void setToken(String token) {
		this.token = token;
	}

	protected String getClientId() {
		return clientId;
	}

	protected void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	protected boolean hasContent() {
		return StringUtils.hasText(getContent());
	}
}
