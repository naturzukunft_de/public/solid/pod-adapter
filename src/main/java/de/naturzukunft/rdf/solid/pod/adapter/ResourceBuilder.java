package de.naturzukunft.rdf.solid.pod.adapter;

public interface ResourceBuilder {

	ResourceBuilder withContent(String content);

	ResourceBuilder withClientId(String clientId);

	ResourceBuilder withToken(String token);

	ResourceBuilder withUrl(String url);

	ResourceBuilder withLink(String link);

	ResourceBuilder withSlug(String slug);

	PodResponse build();

}