package de.naturzukunft.rdf.solid.pod.adapter.oauth2;

import static org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestRedirectFilter.DEFAULT_AUTHORIZATION_REQUEST_BASE_URI;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

public class MyWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

	private final static boolean USE_CUSTOMIZER = true;
	
    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;

    
	@Autowired
	private OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> accessTokenResponseClient;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.anyRequest().authenticated()
				.and()					
			.oauth2Login()
			.authorizationEndpoint().authorizationRequestResolver(resolver());
			
		http
			.oauth2Login()
				.tokenEndpoint()
					.accessTokenResponseClient(accessTokenResponseClient);
	}
	
    protected OAuth2AuthorizationRequestResolver resolver() {
        DefaultOAuth2AuthorizationRequestResolver delegate = new DefaultOAuth2AuthorizationRequestResolver(clientRegistrationRepository, DEFAULT_AUTHORIZATION_REQUEST_BASE_URI);

        // if customizer is set, attributes logged (in line 52) will not contain OAuth2ParameterNames.REGISTRATION_ID
        if (USE_CUSTOMIZER) {
            delegate.setAuthorizationRequestCustomizer(builder -> builder.attributes(map -> {}));
        }

        return new AttributesLoggingResolver(delegate);
    }

	protected OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> getAccessTokenResponseClient() {
		return accessTokenResponseClient;
	}

    static class AttributesLoggingResolver implements OAuth2AuthorizationRequestResolver {
        private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(AttributesLoggingResolver.class);
        private final OAuth2AuthorizationRequestResolver delegate;

        public AttributesLoggingResolver(OAuth2AuthorizationRequestResolver delegate) {
            this.delegate = delegate;
        }

        @Override
        public OAuth2AuthorizationRequest resolve(HttpServletRequest request) {
            OAuth2AuthorizationRequest result = delegate.resolve(request);
            OAuth2AuthorizationRequest r2 = result;
            if (result != null) {
                log.info("attributes: {}", result.getAttributes().keySet());
                
                Map<String, Object> newAttr = new HashMap<>();
                result.getAttributes().keySet().forEach(key -> 
                {
                	if(!"nonce".equals(key)) {
                		newAttr.put(key, result.getAttributes().get(key));
                	}
                });
                
                
                //////////////////////////////////
                // There must be a better solution, see:
                // https://github.com/spring-projects/spring-security/issues/7696
                //////////////////////////////////
                
                r2 = OAuth2AuthorizationRequest.authorizationCode()
        				.authorizationUri(result.getAuthorizationUri())
        				.clientId(result.getClientId())
        				.redirectUri(result.getRedirectUri())
        				.scopes(result.getScopes())
        				.state(result.getState())
        				.additionalParameters(result.getAdditionalParameters())
        				.attributes(newAttr).build();
            }
            return r2;
        }

        @Override
        public OAuth2AuthorizationRequest resolve(HttpServletRequest request, String clientRegistrationId) {
            return delegate.resolve(request, clientRegistrationId);
        }
    }
}
