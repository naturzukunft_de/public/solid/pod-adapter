package de.naturzukunft.rdf.solid.pod.adapter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.security.KeyPair;

import org.junit.jupiter.api.Test;

/**
 * Test Javadoc
 * @author fredy
 *
 */
class TestKeyPairStore {

	@Test
	void test() {
		KeyPairStore ks = new KeyPairStore();
		KeyPair kp = ks.getKeyPair("testuser");
		assertThat(kp, is(ks.getKeyPair("testuser")));
	}
}
